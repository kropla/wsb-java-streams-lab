import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class HelloTest {

    @Test
    void helloTest() {
        var result = new Hello().sayHello();

        assertThat(result).isEqualTo("Hello Student WSB");
    }
}
